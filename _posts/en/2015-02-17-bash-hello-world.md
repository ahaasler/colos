---
title: 'Bash: Hello, World!'
label: post-bash-hello-world
categories: [code]
---

Bash: Hello, World!
===================

```bash
#!/bin/bash
echo "Hello, World!"
```
