---
title: 'Java: Hello, World!'
label: post-java-hello-world
categories: [code]
---

Java: Hello, World!
===================

```java
public class HelloWorld {

	public static void main(String[] args) {
		System.out.println("Hello, World!");
	}

}
```
