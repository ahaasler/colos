---
title: 'Ruby: Hello, World!'
label: post-ruby-hello-world
categories: [code]
---

Ruby: Hello, World!
===================

```ruby
puts 'Hello, World!'
```
