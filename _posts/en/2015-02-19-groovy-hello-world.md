---
title: 'Groovy: Hello, World!'
label: post-groovy-hello-world
categories: [code]
---

Groovy: Hello, World!
=====================

```groovy
println "Hello, World!"
```
