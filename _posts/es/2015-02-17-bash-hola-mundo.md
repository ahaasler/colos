---
title: 'Bash: ¡Hola Mundo!'
label: post-bash-hello-world
categories: [es, codigo]
---

Bash: ¡Hola Mundo!
==================

```bash
#!/bin/bash
echo "¡Hola Mundo!"
```
